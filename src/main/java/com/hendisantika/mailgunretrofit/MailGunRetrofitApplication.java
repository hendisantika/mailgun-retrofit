package com.hendisantika.mailgunretrofit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MailGunRetrofitApplication {

    public static void main(String[] args) {
        SpringApplication.run(MailGunRetrofitApplication.class, args);
    }

}
