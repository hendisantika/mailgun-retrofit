package com.hendisantika.mailgunretrofit.service;

import com.hendisantika.mailgunretrofit.domain.Data;
import com.hendisantika.mailgunretrofit.repository.DataRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : MailGun Retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/21
 * Time: 04.37
 */
@Log4j2
@Service("dataService")
public class DataService {

    @Autowired
    @Qualifier("dataRepository")
    private DataRepository dataRepository;

    public boolean saveData(Data data) {
        try {
            dataRepository.saveEmail(data);
            return true;
        } catch (Exception e) {
            log.error("ERROR DURING SAVE DATA: " + e.getMessage(), e);
            return false;
        }
    }

    public boolean sendData(Data data) {
        try {
            dataRepository.sendEmail(data);
            return true;
        } catch (Exception e) {
            log.error("ERROR DURING SEND DATA: " + e.getMessage(), e);
            return false;
        }
    }

    public Data getEmailByID(int id) {
        return dataRepository.getEmailByID(id);
    }

    public Set<Data> getRandomData() {
        return dataRepository.getRandomData();
    }
}
