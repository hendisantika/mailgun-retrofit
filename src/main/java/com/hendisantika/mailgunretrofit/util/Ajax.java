package com.hendisantika.mailgunretrofit.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : MailGun Retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/04/21
 * Time: 06.00
 */
public class Ajax {
    public static Map<String, Object> successResponse(Object object) {
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("result", "success");
        response.put("query answer time", new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format((new Date()).getTime()));
        response.put("data", object);
        return response;
    }

    public static Map<String, Object> emptyResponse() {
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("query answer time", new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format((new Date()).getTime()));
        response.put("result", "emptyResponse");
        return response;
    }

    public static Map<String, Object> notFoundEmail() {
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("result", "incompleteData");
        response.put("reason", "not found email");
        return response;
    }

    public static Map<String, Object> errorResponse(String errorMessage) {
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("result", "error");
        response.put("message", errorMessage);
        return response;
    }


}
