package com.hendisantika.mailgunretrofit.repository;

import com.hendisantika.mailgunretrofit.config.MongoConnection;
import com.hendisantika.mailgunretrofit.domain.Data;
import com.hendisantika.mailgunretrofit.mailgun.service.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : MailGun Retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 07.14
 */
@org.springframework.stereotype.Repository("dataRepository")
public class DataRepositoryImpl implements DataRepository<Data> {

    private static final Logger LOG = LoggerFactory.getLogger(DataRepositoryImpl.class);
    @Autowired
    protected JdbcOperations jdbcOperations;
    @Value("${spring.data.mongodb.host}")
    private String host;
    @Value("${spring.data.mongodb.port}")
    private String port;
    @Value("${spring.data.mongodb.dbname}")
    private String dbname;
    @Value("${spring.data.mongodb.login}")
    private String login;
    @Value("${spring.data.mongodb.password}")
    private String password;
    @Value("${spring.data.mongodb.table}")
    private String table;
    @Value("${mailgun.data.domain}")
    private String domain;
    @Value("${mailgun.data.key}")
    private String key;
    @Value("${mailgun.data.from}")
    private String from;

    @Override
    public void saveEmail(Data object) {
//        Object[] params = new Object[] { object.getId(), object.getEmail(), object.getDate(), object.getSubject(),
//        object.getMessage() };
//        int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };
//        jdbcOperations.update("INSERT INTO emailtable (data_id, data_email, data_date, data_subject, data_message)
//        VALUES (cast(? as integer), ?, ?, ?, ?);", params, types);

        Properties prop = new Properties();
        prop.setProperty("host", host);
        prop.setProperty("port", port);
        prop.setProperty("dbname", dbname);
        prop.setProperty("login", login);
        prop.setProperty("password", password);
        prop.setProperty("table", table);
        MongoConnection mongoDB = new MongoConnection(prop);

        mongoDB.addEmail(object);

        LOG.error("SAVE EMAIL: " + object);
    }

    @Override
    public Data getEmailByID(int id) {
        Properties prop = new Properties();

        prop.setProperty("host", host);
        prop.setProperty("port", port);
        prop.setProperty("dbname", dbname);
        prop.setProperty("login", login);
        prop.setProperty("password", password);
        prop.setProperty("table", table);

        MongoConnection mongoDB = new MongoConnection(prop);
        LOG.error("FIND EMAIL WHITH ID = " + id);
        return mongoDB.getEmailByID(id);
    }

    @Override
    public void sendEmail(Data object) {
        MailService mailService = new MailService();
        mailService.sendMailMailgun(object.getEmail(), object.getSubject(), object.getMessage(), domain, key, from);
        LOG.error("Send EMAIL WHITH ID = " + object.getId());
    }

    //postgresDB
    @Override
    public void delete(Data object) {
        jdbcOperations.update("DELETE FROM email table\n" +
                " WHERE data_id = '" + object.getId() + "';");
    }

    //postgresDB
    @Override
    public Set<Data> getRandomData() {
        Set<Data> result = new HashSet<>();
        SqlRowSet rowSet = jdbcOperations.queryForRowSet("SELECT * FROM emailtable p ORDER BY RANDOM() LIMIT 50;");
        while (rowSet.next()) {
            result.add(new Data(
                    Integer.parseInt(rowSet.getString("data_id")),
                    rowSet.getString("data_email"),
                    rowSet.getString("data_date"),
                    rowSet.getString("data_subject"),
                    rowSet.getString("data_message")));
        }
        return result;
    }
}
