package com.hendisantika.mailgunretrofit.repository;

import com.hendisantika.mailgunretrofit.domain.Data;
import com.hendisantika.mailgunretrofit.domain.DomainObject;

import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : MailGun Retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 07.13
 */
public interface DataRepository<V extends DomainObject> {

    void saveEmail(V object);

    void sendEmail(V object);

    void delete(V object);

    Data getEmailByID(int id);

    Set<Data> getRandomData();

}