package com.hendisantika.mailgunretrofit.domain;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : MailGun Retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 07.12
 */
@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
public class Data implements DomainObject {

    private int id;
    private String email;
    private String date;
    private String subject;
    private String message;

}
