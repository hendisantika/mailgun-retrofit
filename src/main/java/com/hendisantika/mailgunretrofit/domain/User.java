package com.hendisantika.mailgunretrofit.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : MailGun Retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 07.15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String id;
    private String login;
}
