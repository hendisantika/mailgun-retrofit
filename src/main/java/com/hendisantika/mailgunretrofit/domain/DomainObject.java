package com.hendisantika.mailgunretrofit.domain;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : MailGun Retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 07.11
 */
public interface DomainObject extends Serializable {
}
