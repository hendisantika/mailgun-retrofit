package com.hendisantika.mailgunretrofit.controller;

import com.hendisantika.mailgunretrofit.domain.Data;
import com.hendisantika.mailgunretrofit.service.DataService;
import com.hendisantika.mailgunretrofit.util.Ajax;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : MailGun Retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/21
 * Time: 21.03
 */
@Log4j2
@Controller
public class DataController extends ExceptionHandlerController {

    @Autowired
    @Qualifier("dataService")
    private DataService dataService;

    @PostMapping(value = "/messages/{id}/send")
    public @ResponseBody
    Map<String, Object> messageRecieve(@PathVariable("id") int id, @RequestParam("email") String email,
                                       @RequestParam("subject") String subject,
                                       @RequestParam("message") String message) throws RestException {
        try {
            if (email == null || email.equals("")) {
                return Ajax.notFoundEmail();
            }
            Data data = new Data(id, email,
                    new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format((new Date()).getTime()), subject, message);
            dataService.saveData(data);
            dataService.sendData(data);
            return Ajax.successResponse(data);
        } catch (Exception e) {
            throw new RestException(e);
        }
    }

    @GetMapping(value = "/messages/{id}/show")
    public @ResponseBody
    Map<String, Object> messageRecieve(@PathVariable("id") int id) throws RestException {
        try {
            Data result = dataService.getEmailByID(id);
            return Ajax.successResponse(result);
        } catch (Exception e) {
            throw new RestException(e);
        }
    }

    @GetMapping(value = "/")
    public @ResponseBody
    Map<String, Object> welcome(Map<String, Object> model) {
        log.error("Show main page");
        return Ajax.errorResponse("Index page do not exist. Last try was in " + new SimpleDateFormat("yyyy.MM.dd " +
                "HH:mm").format((new Date()).getTime()));
    }


    @GetMapping(value = "/getRandomData")
    public @ResponseBody
    Map<String, Object> getRandomData() throws RestException {
        try {
            Set<Data> result = dataService.getRandomData();
            return Ajax.successResponse(result);
        } catch (Exception e) {
            throw new RestException(e);
        }
    }

}
