package com.hendisantika.mailgunretrofit.config;

import com.hendisantika.mailgunretrofit.domain.Data;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import java.net.UnknownHostException;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * Project : MailGun Retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 07.17
 */
public class MongoConnection {
    private final MongoClient mongoClient; // this is the client that will provide the connection to the database
    private DB db; // In our case, this class gives // the ability to authenticate to MongoDB
    private boolean authenticate; // here we will store the state of the connection to the database
    private DBCollection table; // And a class that will provide the ability to work // with collections / tables
    // MongoDB

    public MongoConnection(Properties prop) {
        try {
            mongoClient = new MongoClient(prop.getProperty("host"), Integer.valueOf(prop.getProperty("port"))); //
            // Create a connection
            db = mongoClient.getDB(prop.getProperty("dbname")); // Choosing a database for further work
            authenticate = db.authenticate(prop.getProperty("login"), prop.getProperty("password").toCharArray()); //
            // Choosing a database for further work
            table = db.getCollection(prop.getProperty("table")); // Choosing a collection / table for further work
        } catch (UnknownHostException e) {
            System.err.println("Don't connect!");// If you have any problems with the connection, we inform you about
            // it.
        }
    }

    public void addEmail(Data data) {
        BasicDBObject document = new BasicDBObject();
        document.put("data_id", data.getId());
        document.put("data_email", data.getEmail());
        document.put("data_date", data.getDate());
        document.put("data_subject", data.getSubject());
        document.put("data_message", data.getMessage());
        table.insert(document); // writing data to collection / table
    }

    public Data getEmailByID(int id) {
        BasicDBObject query = new BasicDBObject();
        query.put("data_id", id); // задаем поле и значение поля по которому будем искать
        DBObject result = table.findOne(query); // осуществляем поиск
        return new Data(id, String.valueOf(result.get("data_email")), String.valueOf(result.get("data_date")),
                String.valueOf(result.get("data_subject")), String.valueOf(result.get("data_message"))); //
        // we return the received information
    }

    public void updateByLogin(String login, String newLogin) { // login is the old username of the user // newLogin
        // is the new username that we want to set
        BasicDBObject newData = new BasicDBObject();
        newData.put("login", newLogin); // set a new login
        BasicDBObject searchQuery = new BasicDBObject().append("login", login); // specify the updated field and its
        // current value
        table.update(searchQuery, newData); // update the record
    }

    public void deleteByLogin(String login) {
        BasicDBObject query = new BasicDBObject();
        query.put("login", login); // specify which record we will delete from the collection         // setting the
        // field and its current value
        table.remove(query); // remove record from collection / table
    }
}
