package com.hendisantika.mailgunretrofit.mailgun.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by IntelliJ IDEA.
 * Project : MailGun Retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/21
 * Time: 04.43
 */
public interface MailgunApiService {
    @FormUrlEncoded
    @POST("/v3/{domain}/messages")
    Call<ResponseBody> sendEmailMailgun(
            @Header("Authorization") String authorization,
            @Path("domain") String domain,
            @Field("from") String from,
            @Field("to") String to,
            @Field("subject") String subject,
            @Field("text") String text
    );
}