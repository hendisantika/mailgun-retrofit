package com.hendisantika.mailgunretrofit.mailgun.service;


import com.hendisantika.mailgunretrofit.mailgun.exception.MailGunException;

import javax.ws.rs.core.Response;

/**
 * Created by IntelliJ IDEA.
 * Project : MailGun Retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/21
 * Time: 04.46
 */
public class MailgunServiceImpl {
    //https://documentation.mailgun.com/api-intro.html#errors
    private void responseHandler(Response response) {
        int code = response.getStatus();
        if (code != 200) {
            String message;
            switch (code) {
                case 400:
                    message = "400 Bad Request - Required parameter likely missing";
                    break;
                case 401:
                    message = "401 Unauthorized - No Valid API key provided";
                    break;
                case 402:
                    message = "402 Request Failed - Parameters were valid but request failed";
                    break;
                case 404:
                    message = "404 Not Found - The requested item doesn't exist";
                    break;
                case 500:
                case 502:
                case 503:
                case 504:
                    message = code + " Server Error - something is wrong on Mailgun’s end";
                    break;
                default:
                    message = code + " Unknown Error";
                    break;
            }
            throw new MailGunException(message);
        }
    }
}
