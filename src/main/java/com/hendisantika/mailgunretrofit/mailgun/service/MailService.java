package com.hendisantika.mailgunretrofit.mailgun.service;

import lombok.extern.log4j.Log4j2;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.util.Base64;

/**
 * Created by IntelliJ IDEA.
 * Project : MailGun Retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/21
 * Time: 04.41
 */
@Log4j2
public class MailService {
    public void sendMailMailgun(String to, String subject, String text, String domain, String key, String from) {

        try {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.mailgun.net/")
                    .build();

//                    "https://api:key-9578f9ca1c1840c6fa1eee8eae4cc539@api.mailgun.net/v3/sandbox9e3e8a49584244d8a9ab4dcc9491d82f.mailgun.org/log",

            MailgunApiService mailgunApiService = retrofit.create(MailgunApiService.class);

            String clientIdAndSecret = "api" + ":" + key;
            String authorizationHeader = "Basic " + Base64.getEncoder().encodeToString(clientIdAndSecret.getBytes());

            Call<ResponseBody> responseCall = mailgunApiService.sendEmailMailgun(
                    authorizationHeader,
                    domain,
                    from,
                    to,
                    subject,
                    text
            );

            log.warn("Responce from mailgunApi: isExecuted() = " + responseCall.isExecuted() + ", responseCall" +
                    ".toString() = " + responseCall);

//            Response response = responseCall.execute();

            responseCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    log.warn("Responce from mailgunApi:" +
                            "\r isExecuted() = " + responseCall.isExecuted() +
                            ",\n response.message = " + response.message() +
                            ",\r\n response.body = " + response.body() +
                            ",\n response.code = " + response.code() +
                            ",\n response.raw = " + response.raw() +
                            ",\n response.errorBody = " + response.errorBody() +
                            ",\n response.isSuccessful = " + response.isSuccessful() +
                            ",\n response.headers = " + response.headers()
                    );
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                    log.warn("Throwable from mailgunApi: " + throwable.getMessage() + " = " + throwable.fillInStackTrace());
                }

            });

        } catch (IllegalArgumentException e) {
            log.error("ERRROR while sending message " + e);
        }
    }
}
