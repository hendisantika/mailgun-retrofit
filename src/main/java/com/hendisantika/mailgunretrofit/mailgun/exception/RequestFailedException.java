package com.hendisantika.mailgunretrofit.mailgun.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : MailGun Retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/21
 * Time: 20.57
 */
public class RequestFailedException extends RuntimeException {
    public RequestFailedException() {
        super("Request Failed - Parameters were valid but request failed");
    }
}