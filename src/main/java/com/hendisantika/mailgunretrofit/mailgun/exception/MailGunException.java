package com.hendisantika.mailgunretrofit.mailgun.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : MailGun Retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/21
 * Time: 04.47
 */
public class MailGunException extends RuntimeException {

    public MailGunException(String message) {
        super(message);
    }
}
