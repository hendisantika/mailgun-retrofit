package com.hendisantika.mailgunretrofit.mailgun.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : MailGun Retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/21
 * Time: 20.56
 */
public class NotFoundException extends RuntimeException {
    public NotFoundException() {
        super("Not Found - The requested item doesn’t exist");
    }
}